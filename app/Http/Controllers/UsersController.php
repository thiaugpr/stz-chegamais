<?php

namespace App\Http\Controllers;

use App\Mail\UserCreated;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class UsersController extends Controller
{
    public function store(Request $request)
    {
        try {
            $data = $request->only(['email', 'name', 'cpf', 'phone', 'place']);
            $user = new User($data);
            $user->created_at_store = $request->input('inStore');
            $user->categories = rtrim(implode(', ', $request->input('categories')), ', ');
            $user->ip = $_SERVER['REMOTE_ADDR'];
            $user->save();

            Mail::to($user)->send(new UserCreated($user, $request->input('place')));

            return response()->json([
                'success' => true,
            ]);
        } catch (QueryException $e) {
            Log::error('Error in UsersController@store', [$e]);
            $errorCode = $e->errorInfo[1];
            if ($errorCode == 1062) {
                return response()->json([
                    'success' => false,
                    'message' => 'E-mail e/ou CPF já cadastrados.',
                ], 409);
            }

            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 500);
        } catch (\Exception $e) {
            Log::error('Error in UsersController@store', [$e]);
            return response()->json([
                'success' => false,
                'message' => 'Erro inesperado.',
            ], 500);
        }
    }
}
