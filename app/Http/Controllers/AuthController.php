<?php

namespace App\Http\Controllers;

use App\Mail\UserCreated;
use App\Opening;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    public function register(Request $request, $city)
    {
        try {
            $opening = Opening::where('slug', '=', $city)->firstOrFail();

            $indication_code = $request->input('indication_code');

            $parent = null;
            if ($indication_code) {
                $parent = User::where('code', '=', $indication_code)->first();
            }

            $user = User::where('email', '=', $request->input('email'))
                ->where('place', '=', $city)
                ->first();

            if ($user !== null) {
                auth()->loginUsingId($user->id);
                return redirect()->action('HomeController@userPanel', [
                    'city' => $city,
                ]);
            }

            $user = new User();
            $user->email = $request->input('email');
            $user->place = $city;
            $user->created_at_store = ($request->input('inStore') === 'true');
            $user->code = $this->generateRandomString(16);
            $user->ip = $_SERVER['REMOTE_ADDR'];
            $user->indicated_by = $parent ? $parent->id : null;
            $user->save();

            auth()->loginUsingId($user->id);

            return redirect()->action('HomeController@userPanel', [
                'city' => $opening->slug,
            ]);
        } catch (ModelNotFoundException $e) {
            return redirect()->back();
        } catch (\Exception $e) {
            Log::error('Error in AuthController@login', [$e]);
        }
    }

    public function checkCPF($cpf)
    {
        $cpf_count = User::where('cpf', '=', $cpf)->count();

        if ($cpf_count > 0) {
            return response()->json([
                'success' => false,
            ], 409);
        }

        return response()->json(['success' => true]);
    }

    public function finishRegister(Request $request, $city)
    {
        $opening = Opening::where('slug', '=', $city)->firstOrFail();
        $user = auth()->user();

        $user->name = $request->input('name');
        $user->cpf = $request->input('cpf');
        $user->phone = $request->input('phone');
        $user->categories = rtrim(implode(', ', $request->input('categories')), ', ');
        $user->save();

        Mail::to($user)->send(new UserCreated($user, $city));

        return redirect()->action('HomeController@userPanel', [
            'city' => $opening->slug,
        ]);
    }

    public function login(Request $request)
    {
        $slug = $request->input('place');
        $email = $request->input('email');

        try {
            $user = User::where('email', '=', $email)->where('place', '=', $slug)->firstOrFail();
            $opening = Opening::where('slug', '=', $slug)->firstOrFail();
            auth()->loginUsingId($user->id);

            return redirect()->action('HomeController@userPanel', [
                'city' => $opening->slug,
            ]);
        } catch (ModelNotFoundException $e) {
            return redirect()->back()->with('error', 'E-mail não cadastrado.');
        } catch (\Exception $e) {
            Log::error('Error in AuthController@login', [$e]);
        }
    }

    private function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
