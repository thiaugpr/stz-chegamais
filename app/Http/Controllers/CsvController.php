<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class CsvController extends Controller
{
    public function index($city)
    {
        $users = User::wherePlace($city)->get();

        if ($users->count() === 0) {
            abort(404);
        }

        $columns = ['LastName', 'Email', 'CPF', 'City', 'Category', 'discount', 'createStore', 'Mobile', 'OptinDate'];
        $callback = function () use ($users, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);
            foreach ($users as $user) {
                fputcsv($file, [
                    $user->name,
                    $user->email,
                    $user->cpf,
                    $user->place,
                    str_replace(',', ';', $user->categories),
                    $this->calculateDiscount($user),
                    $user->created_at_store ? 'S' : 'N',
                    $user->phone ? str_replace([' ', '(', ')', '-'], [''], '55' . $user->phone) : null,
                    $user->created_at->format('d/m/Y H:i:s'),
                ]);
            }
            fclose($file);
        };

        $headers = array(
            'Content-type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename=' . $city . '-'. date('Ymd') . '.csv',
            'Pragma' => 'no-cache',
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Expires' => '0'
        );
        return response()->stream($callback, 200, $headers);
    }

    private function calculateDiscount($user)
    {
        $discount = 0;
        if ($user->cpf) {
            $discount += 20;
        }

        $indications_count = User::where('indicated_by', '=', $user->id)->count();

        if ($indications_count > 0) {
            $discount += 10;
        }

        return $discount;
    }
}
