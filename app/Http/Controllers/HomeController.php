<?php

namespace App\Http\Controllers;

use App\Opening;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index($city = null, $indicationCode = null)
    {
        try {
            $opening = Opening::where('slug', '=', $city)
                ->firstOrFail();

            if ($opening->date->startOfDay()->subDay(2) <= Carbon::now()) {
                return view('home.finished');
            }

            auth()->logout();
            return view('home.index')->with([
                'opening' => $opening,
                'indicationCode' => $indicationCode,
            ]);
        } catch (ModelNotFoundException $e) {
            return view('home.404');
        } catch (\Exception $e) {
            return view('home.404');
        }
    }

    public function userPanel($city)
    {
        if (auth()->check() === false) {
            return redirect('/' . $city);
        };

        try {
            $opening = Opening::where('slug', '=', $city)
                ->firstOrFail();

            $discount = 0;
            $indications = User::where('indicated_by', '=', auth()->user()->id)->count();
            if ($indications > 1) {
                $indications = 1;
            }

            $discount += $indications * 10;

            if (auth()->user()->cpf !== null) {
                $discount += 20;
            } else {
                $discount = 0;
            }

            return view('home.panel')->with([
                'opening' => $opening,
                'discount' => $discount
            ]);
        } catch (ModelNotFoundException $e) {
            return view('home.404');
        } catch (\Exception $e) {
            return view('home.404');
        }
    }
}
