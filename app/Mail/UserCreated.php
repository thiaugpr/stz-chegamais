<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserCreated extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $place;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $place)
    {
        $this->user = $user;
        $this->place = $place;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Cadastro finalizado - Chega mais, STZ')
            ->view('emails.' . $this->place)
            ->with([
                'user' => $this->user
            ]);
    }
}
