<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use SoftDeletes;

    protected $fillable = [
        'email', 'name', 'cpf', 'phone', 'category', 'ip', 'place',
    ];

    protected $dates = [
        'created_at'
    ];
}
