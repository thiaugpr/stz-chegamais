<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta property="og:url" content="{{ url()->current() }}" />
        <meta property="og:title" content="@yield('title', 'STZ chegou!')" />
        <meta property="og:description" content="@yield('description', 'E aí, que tal descolar até 30% de desconto na inauguração da maior fast fashion de calçados do Brasil?')" />
        <meta property="og:image" content="https://www.stzchegou.com.br/img/og-image.png" />
        <meta property="og:image:width" content="1200" />
        <meta property="og:image:height" content="628" />
        <title>Chega mais, STZ</title>
        <link rel="stylesheet" href="/css/app.css?v=3.1.2">
        @include('partials.social-analytics')
    </head>
    <body>
        <div id="app">
            @yield('content')
        </div>
        <script src="/js/app.js?v=3.1.2"></script>
        @yield('extra-script')
        @include('partials.notification')
    </body>
</html>
