<html>
<head>
<title>Cadastro Salvador</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<center>
<table width="640" height="1180" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="6">
			<img style="display: block;" src="{{ asset('/img/emails/salvador') }}/salvador_01.gif" width="640" height="82" alt=""></td>
	</tr>
	<tr>
		<td colspan="6">
			<img style="display: block;" src="{{ asset('/img/emails/salvador') }}/salvador_02.gif" width="640" height="100" alt=""></td>
	</tr>
	<tr>
		<td colspan="6">
			<img style="display: block;" src="{{ asset('/img/emails/salvador') }}/salvador_03.gif" width="640" height="39" alt=""></td>
	</tr>
	<tr>
		<td colspan="6">
			<img style="display: block;" src="{{ asset('/img/emails/salvador') }}/salvador_04.gif" width="640" height="84" alt=""></td>
	</tr>
	<tr>
		<td colspan="6">
			<img style="display: block;" src="{{ asset('/img/emails/salvador') }}/salvador_05.gif" width="640" height="30" alt=""></td>
	</tr>
	<tr>
		<td colspan="6">
			<img style="display: block;" src="{{ asset('/img/emails/salvador') }}/salvador_06.gif" width="640" height="38" alt=""></td>
	</tr>
	<tr>
		<td colspan="6">
			<img style="display: block;" src="{{ asset('/img/emails/salvador') }}/salvador_07.gif" width="640" height="65" alt=""></td>
	</tr>
	<tr>
		<td colspan="6">
			<img style="display: block;" src="{{ asset('/img/emails/salvador') }}/salvador_08.gif" width="640" height="325" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img style="display: block;" src="{{ asset('/img/emails/salvador') }}/salvador_09.gif" width="354" height="114" alt=""></td>
		<td colspan="2">
			<img style="display: block;" src="{{ asset('/img/emails/salvador') }}/salvador_10.gif" width="220" height="114" alt=""></td>
		<td>
			<img style="display: block;" src="{{ asset('/img/emails/salvador') }}/salvador_11.gif" width="66" height="114" alt=""></td>
	</tr>
	<tr>
		<td colspan="6">
			<img style="display: block;" src="{{ asset('/img/emails/salvador') }}/salvador_12.jpg" width="640" height="115" alt=""></td>
	</tr>
	<tr>
		<td colspan="6">
			<img style="display: block;" src="{{ asset('/img/emails/salvador') }}/salvador_13.gif" width="640" height="101" alt=""></td>
	</tr>
	<tr>
		<td>
			<img style="display: block;" src="{{ asset('/img/emails/salvador') }}/salvador_14.gif" width="217" height="86" alt=""></td>
		<td>
			<a href="https://stz.com.br">
				<img style="display: block;" src="{{ asset('/img/emails/salvador') }}/Salvador_Emailmarketing_V2-1_15.gif" width="97" height="86" border="0" alt=""></a></td>
		<td colspan="2">
			<a href="https://facebook.com/studiozcalcados">
				<img style="display: block;" src="{{ asset('/img/emails/salvador') }}/Salvador_Emailmarketing_V2-1_16.gif" width="151" height="86" border="0" alt=""></a></td>
		<td colspan="2">
			<a href="https://instagram.com/studiozcalcados">
				<img style="display: block;" src="{{ asset('/img/emails/salvador') }}/Salvador_Emailmarketing_V2-1_17.jpg" width="175" height="86" border="0" alt=""></a></td>
	</tr>
	<tr>
		<td>
			<img style="display: block;" src="{{ asset('/img/emails/salvador') }}/spacer.gif" width="217" height="1" alt=""></td>
		<td>
			<img style="display: block;" src="{{ asset('/img/emails/salvador') }}/spacer.gif" width="97" height="1" alt=""></td>
		<td>
			<img style="display: block;" src="{{ asset('/img/emails/salvador') }}/spacer.gif" width="40" height="1" alt=""></td>
		<td>
			<img style="display: block;" src="{{ asset('/img/emails/salvador') }}/spacer.gif" width="111" height="1" alt=""></td>
		<td>
			<img style="display: block;" src="{{ asset('/img/emails/salvador') }}/spacer.gif" width="109" height="1" alt=""></td>
		<td>
			<img style="display: block;" src="{{ asset('/img/emails/salvador') }}/spacer.gif" width="66" height="1" alt=""></td>
	</tr>
</table>
</center>
</body>
</html>