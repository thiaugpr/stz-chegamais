<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>STZ Calçados</title>
    </head>
    <body>
        <p>Usuário cadastrado com sucesso =]</p>
        <p>Dados:</p>
        <ul>
            <li>Nome: {{ $user->name }}</li>
            <li>E-mail: {{ $user->email }}</li>
            <li>CPF: {{ $user->cpf }}</li>
            <li>Categorias: {{ $user->categories }}</li>
            <li>Telefone: {{ $user->phone }}</li>
            <li>IP: {{ $user->ip }}</li>
        </ul>
    </body>
</html>