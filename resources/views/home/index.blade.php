@extends('layouts.master')

@section('title', 'STZ chegou, ' . $opening->city)

@if($indicationCode)
    @section('description', 'E aí, que tal descolar até 30% de desconto na inauguração da maior fast fashion de calçados do Brasil? Cadastre-se pelo meu link, chame seus amigos também e chega mais!')
@endif

@section('content')
    @include('partials.intro')
    <div class="flex">
        <countdown date="{{ $opening->date }}" place="{{ $opening->place }}"></countdown>
        <email-form place="{{ $opening->slug }}" :in-store="{{ app('request')->input('loja') ? 'true' : 'false' }}" indication-code="{{ $indicationCode }}"></email-form>
    </div>
    <custom-map :lat="{{ $opening->lat }}" :lng="{{ $opening->lng }}"></custom-map>
    <div class="info">
        <div class="instructions">
            Que tal garantir <strong>30% de desconto</strong> em todos os itens<br class="mobile-only">
            da <strong>STZ</strong> no dia da inauguração? Você pode!<br class="mobile-only">
            Cadastre-se acima e <strong>receba<br class="mobile-only">
            até 30% OFF</strong> no dia da inauguração.
        </div>
        <div class="hashtag">#CrieAModaSTZ</div>
        @include('partials.social')
    </div>
    @include('partials.footer')
@endsection