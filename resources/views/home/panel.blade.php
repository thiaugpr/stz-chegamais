@extends('layouts.master')

@section('title', 'STZ chegou, ' . $opening['city'])

@section('content')
    <div class="intro-light">
        <a href="/{{ $opening['slug'] }}">
            <div class="logo">
                <img src="/img/logo-stz.png" alt="Chega Mais, StudioZ">
                <div class="city">{{ $opening['city'] }}</div>
            </div>
        </a>
        <div class="greetings">
            <p><strong>Olá!</strong> {{ explode(' ', auth()->user()->name)[0] }}</p>
        </div>
    </div>
    <countdown2 date="{{ $opening['date'] }}" place="{{ $opening['place'] }}"></countdown2>
    <indications :discount="{{ $discount }}"></indications>
    <invite-friends share-link="{{ env('APP_URL') }}/{{ $opening['slug'] }}/{{ auth()->user()->code }}"></invite-friends>
    <div class="info">
        <div class="flex">
            <div class="instructions panel">
                Ao se cadastrar, você garante <strong>20% de desconto</strong>. Quando o primeiro amigo
                realizar o cadastro através do seu convite, você garante <strong>mais 10% de desconto</strong>.<br>
                Chame quantos amigos quiser! Mas, lembre-se, apenas o primeiro cadastro realizado
                através do seu convite irá contabilizar, adicionando até 10% de desconto.<br>
                No dia da inauguração, seu desconto estará pronto para uso, <strong>basta informar
                seu CPF na hora da compra</strong>.<br>
                <u>Confira as regras de desconto!</u>
            </div>
            <div class="coupon mobile-only">
                <img src="/img/coupon.png" alt="Cupom">
            </div>
            <div class="coupon desktop-only">
                <img src="/img/coupon-desktop.png" alt="Cupom">
            </div>
        </div>
        @include('partials.social')
    </div>
    @if(!auth()->user()->cpf)
        <finish-register city="{{ $opening['slug'] }}" v-cloak></finish-register>
    @endif
    @include('partials.footer')
@endsection
