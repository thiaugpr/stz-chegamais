<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Chega mais #stz</title>
        <link rel="stylesheet" href="/css/app.css">
        @include('partials.social-analytics')
    </head>
    <body>
        <div id="app">
            <div class="error is-404">
                <p class="image"><img src="/img/logo-chega-mais.png" alt="Chega mais, STZ"></p>
                <p class="error-number">404</p>
                <p class="error-text">Página não encontrada</p>
            </div>
        </div>
        <script src="/js/app.js"></script>
    </body>
</html>