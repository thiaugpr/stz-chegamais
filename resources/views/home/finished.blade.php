@extends('layouts.master')

@section('content')
    <div class="error">
        <p class="image"><img src="/img/logo-chega-mais.png" alt="Chega mais, STZ"></p>
        <p class="error-number">Promoção encerrada</p>
        <p class="error-text">Desculpe, mas essa promoção já foi encerrada.</p>
    </div>
@endsection