@extends('layouts.master')

@section('content')
    <div class="error">
        <p class="image"><img src="/img/logo-chega-mais.png" alt="Chega mais, STZ"></p>
        <p class="error-number">404</p>
        <p class="error-text">Página não encontrada.</p>
    </div>
@endsection