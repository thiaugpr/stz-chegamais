@if(session('message'))
    <script>
    $toast.show({
        text: '{!! nl2br(session('message')) !!}',
        type: 'success',
    });
    </script>
@elseif(session('error'))
    <script>
    $toast.show({
        text: '{!! nl2br(session('error')) !!}',
        type: 'error',
    });
    </script>
@endif
