<div class="intro">
    <div class="logo">
        <img src="/img/logo-chega-mais.png" alt="Chega Mais, StudioZ">
    </div>
    @if(!auth()->check())
        <div class="login-form">
            <form method="POST" action="/login">
                <p>Já é cadastrado?</p>
                <label for="email">Login:</label>
                <input type="hidden" name="place" value="{{ $opening['slug'] }}">
                <div class="group">
                    <input type="email" name="email" placeholder="Seu e-mail" />
                    <button type="submit" class="desktop-only">Entrar</button>
                </div>
            </form>
        </div>
    @endif
    <div class="location">
        <img src="/img/city/{{ $opening['slug'] }}.png" alt="STZ em {{ $opening['city'] }}">
    </div>
    <div class="discount">
        <img src="/img/30-off.png" alt="30% off, você quer?">
    </div>
    <div class="arrow-down">
        <img src="/img/arrow-down.png" alt="Seta para baixo">
    </div>
    <div class="play">
        <button type="button" @click="isVideoVisible = true"><font-awesome-icon :icon="['fas', 'play']"></font-awesome-icon></button>
    </div>
    <div class="modal" v-if="isVideoVisible" @click.stop="isVideoVisible = false" v-cloak>
        <div class="content">
            <button type="button" class="close"><font-awesome-icon :icon="['fas', 'times']"></font-awesome-icon></button>
            <div class="video">
                <iframe src='https://www.youtube.com/embed/z7i3gcC3LlE?autoplay=1' frameborder='0' allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>