<div class="intro-light">
    <div class="logo">
        <img src="/img/logo-stz.png" alt="Chega Mais, StudioZ">
        <div class="city">{{ $opening['city'] }}</div>
    </div>
    <div class="greetings">
        @if(auth()->check())
            <p><strong>Olá!</strong> {{ explode(' ', auth()->user()->name)[0] }}</p>
        @else
            <p><strong>Olá!</strong></p>
        @endif
    </div>
</div>