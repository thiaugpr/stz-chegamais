<div class="social">
    <hr>
    <div class="whatsapp">
        <div class="wa-text">Nos dê um oi no WhatsApp e receba as próximas novidades e promoções!</div>
        <div class="icon">
            <a href="http://bit.ly/STZ-Shopping-Patteo-Olinda" target="_blank">
                <img src="/img/icon-whatsapp.png" alt="WhatsApp">
            </a>
        </div>
    </div>
    <div class="text">Fique por dentro das novidades</div>
    <ul class="social-network-list">
        <li>
            <div class="icon"></div>
            <div class="type">Site</div>
            <div class="name"><a href="https://stz.com.br" target="_blank">stz.com.br</a></div>
        </li>
        <li>
            <div class="icon"></div>
            <div class="type">Facebook</div>
            <div class="name"><a href="https://facebook.com/studiozcalcados" taget="_blank">studiozcalcados</a></div>
        </li>
        <li>
            <div class="icon"></div>
            <div class="type">Instagram</div>
            <div class="name"><a href="https://www.instagram.com/studiozcalcados" target="_blank">@studiozcalcados</a></div>
        </li>
    </ul>
</div>