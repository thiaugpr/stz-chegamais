// packages
import Vue from 'vue';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/pro-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

// components
import Toast from './components/Toast';
import Countdown from './components/Countdown.vue';
import Countdown2 from './components/Countdown2.vue';
import CustomMap from './components/Map.vue';
import EmailForm from './components/EmailForm.vue';
import FinishRegister from './components/FinishRegister.vue';
import Indications from './components/Indications.vue';
import InviteFriends from './components/InviteFriends.vue';

library.add(fas);
Vue.component('FontAwesomeIcon', FontAwesomeIcon);

Vue.use(Toast);

const vue = new Vue({
  el: '#app',
  components: { Countdown, Countdown2, CustomMap, FinishRegister, Indications, InviteFriends, EmailForm },
  data() {
    return {
      isVideoVisible: false,
      isRegisterVisible: false,
    };
  },
});
