import ToastList from './ToastList.vue';

export default {
  install(Vue) {
    const ComponentClass = Vue.extend(ToastList);
    const instance = new ComponentClass();

    Vue.prototype.$toast = instance;
    window.$toast = instance;
  },
};
/*
import ToastList from './ToastList.vue';

export default {
  install(Vue) {
    // Merge options argument into options defaults
    const vue = new Vue({
      render: createElement => createElement(ToastList),
    });

    // add component to DOM
    vue.$mount(document.body.appendChild(document.createElement('div')));

    [Vue.prototype.$toast] = vue.$children;
    [window.$toast] = vue.$children;
  },
};
*/
