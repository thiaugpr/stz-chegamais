class Timer {
  /**
   * Use it just like a normal setTimeout function.
   *
   * @param {function} callback callback function
   * @param {number} delay delay in ms
   */
  constructor(callback, delay) {
    this.callback = callback;
    this.delay = delay;
    this.remaining = this.delay;
    this.resume();
  }

  /**
   * Pause the timer.
   */
  pause() {
    window.clearTimeout(this.timerId);
    this.remaining -= new Date() - this.start;
  }

  /**
   * Start/resume the timer.
   */
  resume() {
    this.start = new Date();
    window.clearTimeout(this.timerId);
    this.timerId = window.setTimeout(this.callback, this.remaining);
  }

  getRemaining() {
    this.remaining -= new Date() - this.start;
    return this.remaining;
  }
}

export default Timer;
