<?php

Route::post('/login', 'AuthController@login');

Route::get('/{city}/meu-painel', 'HomeController@userPanel');
Route::get('/{city}/download-csv', 'CsvController@index');
Route::post('/{city}/register', 'AuthController@register');
Route::post('/{city}/finish-register', 'AuthController@finishRegister');

Route::get('/{city?}/{indicationCode?}', 'HomeController@index');
