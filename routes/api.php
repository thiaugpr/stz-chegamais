<?php

Route::post('/users', 'UsersController@store');
Route::get('/users/check-cpf/{cpf}', 'AuthController@checkCPF');
