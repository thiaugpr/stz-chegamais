module.exports = {
  singleQuote: true,
  printWidth: 120,
  quoteProps: 'consistent',
  trailingComma: 'es5',
  useTabs: false,
};

