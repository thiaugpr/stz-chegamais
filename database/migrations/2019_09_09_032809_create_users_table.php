<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('email');
            $table->string('cpf')->nullable();
            $table->string('place');
            $table->string('categories')->nullable();
            $table->boolean('created_at_store');
            $table->string('phone')->nullable();
            $table->string('ip');
            $table->string('code');
            $table->unsignedBigInteger('indicated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('indicated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
