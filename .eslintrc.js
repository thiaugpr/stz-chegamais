module.exports = {
    extends: ['airbnb'],
    plugins: [],
    parserOptions: {
        ecmaVersion: 8,
    },
    env: {
        browser: true,
        jasmine: true,
        jest: true,
    },
    rules: {
        'arrow-parens': [2, 'as-needed'],
        'import/no-extraneous-dependencies': [2, { devDependencies: true }],
        'indent': [2, 2],
        'max-len': [2, { code: 120 }],
        'no-confusing-arrow': [0],
        'no-console': [0],
        'no-param-reassign': [2, { props: false }],
        'no-plusplus': [0],
        'no-unused-expressions': [2, { allowTaggedTemplates: true }],
        // -- REACT --
        // 'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
        // 'react/no-this-in-sfc': [0],
        // 'react/jsx-indent': [2, 4],
        // 'react/jsx-indent-props': [2, 4],
        // -- VUE.JS --
        // 'vue/attributes-order': [0],
        // 'vue/html-indent': [2, 4],
        // 'vue/html-self-closing': [
        //     1,
        //     {
        //         html: {
        //             void: 'never',
        //             normal: 'any',
        //             component: 'always',
        //         },
        //         svg: 'always',
        //         math: 'always',
        //     },
        // ],
        // 'vue/multiline-html-element-content-newline': [1, {
        //     'ignoreWhenEmpty': true,
        //     'ignores': ['pre', 'textarea', 'p', 'span', 'li', 'td', 'th'],
        // }],
        // 'vue/singleline-html-element-content-newline': [0],
    },
};
